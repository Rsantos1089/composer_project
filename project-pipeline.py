# requirements2.txt
# apache-airflow-google
# gcloud composer environments update composer-datahack --update-pypi-packages-from-file requirements.txt --location us-central1
# STEP 1: Libraries needed
from datetime import timedelta, datetime
from airflow import models
from airflow.contrib.operators import dataproc_operator
from airflow.utils import trigger_rule
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.utils import trigger_rule

#from airflow.providers.google.cloud.operators.datafusion import CloudDataFusionStartPipelineOperator

# STEP 2:Define a start date
yesterday = datetime(2020, 6, 6)#year/month/day -

# STEP 3: Set default arguments for the DAG
default_dag_args = {
'start_date': yesterday,
'depends_on_past': False,
'email_on_failure': False,
'email_on_retry': False,
'retries': 0,
'retry_delay': timedelta(minutes=5)
}
# STEP 4: Define DAG
with models.DAG(
'project_pipeline',
description='Data Pipeline with Dataproc and Data Fusion',
schedule_interval='@daily',
default_args=default_dag_args) as dag:

# STEP 5: Set Operators
    run_workflow_dataproc = dataproc_operator.DataprocWorkflowTemplateInstantiateOperator(
      task_id='run_workflow_dataproc',
      template_id='data-enrichment-covid-enero',
      project_id='de-program-team-d',
      region='us-central1'
   )
    
    #run_datafusion_pipeline = CloudDataFusionStartPipelineOperator(
     # location='us-west1',
      #pipeline_name='data_enrichment_pipeline',
      #instance_name='datahack-datafusion',
      #task_id="run_datafusion_pipeline",
   #)

    run_datafusion_pipeline = SimpleHttpOperator(
      task_id='run_datafusion_pipeline',
      http_conn_id='datafusion-enero',
      endpoint='/v3/namespaces/default/apps/data_enrichment_pipeline-enero/workflows/DataPipelineWorkflow/start',
      method='POST',
      headers={'Authorization': 'Bearer ya29.a0AfH6SMAzrbrIdlcG5TFVqSm073loFYXvxBrURwj4YBN1NCiJ85YCCS0HeFwPtiDEVsR2l7PHxlpGcn50G3r2eyDCTfr5S_2lDacJ6BEWDhUt8P9vpp6IWfvnWebwq5yMHO5rVtfTmKMYUhEIRZQDYN5FmO7F5KGV8DZ7WI-BxZdYvfG7eHPMYtWSZockew9k8sWvz_2jL_C4j3k5gvPKsfUsmOpA-jA9eBib4rWkI7DHKmHoaN_xEWsm_eMdpQFWXQYZET6u2QvoVw'},
      trigger_rule=trigger_rule.TriggerRule.ALL_DONE,
      dag=dag
   )

# STEP 6: Set DAGs dependencies
    run_workflow_dataproc >> run_datafusion_pipeline